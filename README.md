## Projeto Flowing


Este projeto trata-se de um sistema de controle de fluxo e mapeando, com aplicação inicial na biblioteca Nilo Peçanha localizada no ifpb.
A biblioteca do IFPB, conta com áreas de estudo: coletivo, individual, virtual (com computadores) e sala para estudo em grupo. 
Por questões de segurança e organização é necessário que os estudantes, ao terem acesso ao acervo e áreas de estudo na biblioteca guardem os seus pertences,
como bolsas, dentro de armários que ficam no hall de entrada da mesma. Porém, surge um certo incômodo quando depois de guardar seus materiais e subir à área de estudo,
o estudante se depara com um ambiente cheio e sem lugares disponíveis. Pensando nisso, pensamos em projetar um sistema de mapeamento para essas áreas,
que mostraria os lugares ocupados na biblioteca, estatísticas de fluxo e horários reservados para a sala de estudo em grupo. 
Essas informações seriam exibidas em um monitor no hall de entrada da biblioteca e estariam na forma de mapa – no caso dos lugares, 
que mudariam de cor de acordo com a disponibilidade – e gráficos. Posteriormente, pretende-se fazer com que esses dados sejam visualizados por aplicativo 
ou no próprio site da biblioteca, garantindo maior praticidade aos alunos. Seria incrementado também um sistema assistivo para deficientes visuais, 
onde os dados seriam exibidos dinamicamente por áudio. 


## Requisitos
    
    Prototipo:
        ESP88266 NodeMCU;
        6 push button;
        2 TCRT5000;
        6 resistores de 10 kohms;
        1 resistor de 330 ohms;
        1 resistor de 1 kohm;
        1 protoboard;
        n jumpers macho x macho;
    
    Maquina
        Xampp : apache , mysql;
        Arduino IDE;
        Visual Studio Code;
        Web browser;
    
## Banco de dados:

create database biblioteca_db;
use biblioteca_db;
#creating tables:
create table sensores(
	sensor_number int not null primary key,
	sensor_name varchar(100) not null   
);

create table sensor_log(
	id int not null auto_increment primary key,
    sensor_number int,
    estado varchar(100) not null,
    data date not null,
    hora time not null,
    foreign key (sensor_number) references
    sensores(sensor_number)
);
    
create table log_de_fluxo(
	id int not null auto_increment primary key,
    status varchar(100) not null,
    data date not null,
    hora time not null,
    c_entrada int not null,
    c_saida	int not null
);

create table mesas(
	id_mesa varchar(50) not null primary key,
    description varchar(100) not null
);

create table cadeiras(
	id_cadeira varchar(50) primary key,
    mesa varchar(50),
	description varchar(200) not null,
    foreign key(mesa) references 
    mesas(id_mesa)
);

create table status_cadeiras(
	id int not null primary key auto_increment,
    cadeira varchar(50),
    status varchar(100),
    foreign key(cadeira) references
    cadeiras(id_cadeira)
);

create table usuarios(
	id int not null auto_increment primary key,
    login varchar(100) not null,
    senha varchar(100) not null
);


## Screenshot
![tela de Login](Screenshot/login.png)
![Tela inicial](Screenshot/tela_inicial.png)
![menu](Screenshot/menu.png)

    